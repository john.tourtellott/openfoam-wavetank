cmake_minimum_required(VERSION 3.12)

#-----------------------------------------------------------------------------
# Project name and version
project(openfoam-wavetank VERSION 1.0)

#-----------------------------------------------------------------------------
# Plugin options
# option(BUILD_SHARED_LIBS  "Build CMB using shared libraries" ON)
# option(BUILD_EXAMPLES "Build example applications" ON)

# Set the directory where the binaries will be stored
# include(GNUInstallDirs)
# set(EXECUTABLE_OUTPUT_PATH         ${CMAKE_BINARY_DIR}/${CMAKE_INSTALL_BINDIR})
# set(CMAKE_RUNTIME_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR}/${CMAKE_INSTALL_BINDIR})
# set(CMAKE_LIBRARY_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR}/${CMAKE_INSTALL_LIBDIR})
# set(CMAKE_INSTALL_CMAKEDIR "${CMAKE_INSTALL_LIBDIR}/cmake/${PROJECT_NAME}")

# Add our Cmake directory to the module search path
list(APPEND CMAKE_MODULE_PATH ${CMAKE_MODULE_PATH} ${PROJECT_SOURCE_DIR}/cmake)

option(ENABLE_TESTING "Enable Testing" OFF)
if (ENABLE_TESTING)
  include(CTest)
  include(TestingMacros)
  enable_testing()
endif()

# if(WIN32 AND MSVC)
#   #setup windows exception handling so we can compile properly with boost
#   #enabled
#   set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} /EHsc")
#   #quiet warnings about printf being potentially unsafe
#   set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} /wd4996")
#   #quiet warnings about truncating decorated name
#   set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} /wd4503")
#   add_definitions(-DBOOST_ALL_NO_LIB)
# endif()

# Find smtk
find_package(smtk REQUIRED)

if (NOT SMTK_ENABLE_PYTHON_WRAPPING)
  message(FATAL, "SMTK_ENABLE_PYTHON_WRAPPING is off; cannot configure.")
endif ()

# Build libs
add_subdirectory(smtk)

# BUILD_DOCUMENTATION is an enumerated option:
# never  == No documentation, and no documentation tools are required.
# manual == Only build when requested ("ninja doc-user"); documentation
#           tools (sphinx) must be located during configuration.
# always == Build documentation as part of the default target; documentation
#           tools are required. This is useful for automated builds that
#           need "make; make install" to work, since installation will fail
#           if no documentation is built.
#   set(BUILD_DOCUMENTATION
#   "never" CACHE STRING "When to build Sphinx-generated documentation.")
# set_property(CACHE BUILD_DOCUMENTATION PROPERTY STRINGS never manual always)

# # Note: Set SPINX_EXECUTABLE (advanced variable) if Sphinx installed in nonstandard location
# if (NOT BUILD_DOCUMENTATION STREQUAL "never")
#   find_package(Sphinx)
# endif()

# if (NOT SMTK_ENABLE_PARAVIEW_SUPPORT)
#   message("Not building plugin -- SMTK not built with ParaView support.")
# else ()
#   # Build the plugin
#   paraview_plugin_scan(
#     PLUGIN_FILES plugin/paraview.plugin
#     PROVIDES_PLUGINS paraview_plugins
#     ENABLE_BY_DEFAULT ON
#     HIDE_PLUGINS_FROM_CACHE ON)

#   set(autoload_args AUTOLOAD)

#   option(ENABLE_PLUGIN_BY_DEFAULT "Automatically enable the plugin in CMB modelbuilder" OFF)
#   if (ENABLE_PLUGIN_BY_DEFAULT)
#     list(APPEND autoload_args ${paraview_plugins})
#   endif ()

#   string(REPLACE "-" "_" safe_project_name "${PROJECT_NAME}")

#   paraview_plugin_build(
#     LIBRARY_SUBDIRECTORY "smtk-${smtk_VERSION}"
#     PLUGINS ${paraview_plugins}
#     PLUGINS_FILE_NAME "smtk.wavetank.xml"
#     ${autoload_args}
#     INSTALL_EXPORT ${PROJECT_NAME}-plugin
#     HEADERS_DESTINATION "${CMAKE_INSTALL_INCLUDEDIR}/${PROJECT_NAME}/${PROJECT_VERSION}"
#     CMAKE_DESTINATION "${CMAKE_INSTALL_CMAKEDIR}"
#     ADD_INSTALL_RPATHS ON
#     TARGET ${safe_project_name}_paraview_plugins)

#   include(CMakePackageConfigHelpers)

#   # Our requirements for a version file are basic, so we use CMake's basic version
#   # file generator
#   write_basic_package_version_file(
#     "${CMAKE_CURRENT_BINARY_DIR}/${PROJECT_NAME}/${PROJECT_NAME}-config-version.cmake"
#     VERSION ${${PROJECT_NAME}_VERSION}
#     COMPATIBILITY AnyNewerVersion
#   )

#   export(
#     EXPORT ${PROJECT_NAME}
#     FILE   "${CMAKE_BINARY_DIR}/${CMAKE_INSTALL_CMAKEDIR}/${PROJECT_NAME}-targets.cmake"
#   )
#   install(
#     EXPORT      ${PROJECT_NAME}
#     DESTINATION "${CMAKE_INSTALL_CMAKEDIR}"
#     FILE        "${PROJECT_NAME}-targets.cmake")

#   configure_file(
#     "${CMAKE_CURRENT_SOURCE_DIR}/cmake/smtk-plugin-config.cmake.in"
#     "${CMAKE_BINARY_DIR}/${CMAKE_INSTALL_CMAKEDIR}/${PROJECT_NAME}-config.cmake"
#     @ONLY)
#   install(
#     FILES       "${CMAKE_BINARY_DIR}/${CMAKE_INSTALL_CMAKEDIR}/${PROJECT_NAME}-config.cmake"
#     DESTINATION "${CMAKE_INSTALL_CMAKEDIR}")

#   # Install workflow files
#   install(
#     DIRECTORY "${CMAKE_SOURCE_DIR}/simulation-workflows/"
#     DESTINATION "${SIMULATION_WORKFLOWS_ROOT}/workflows"
#   )

#   # Install python operations
#   install(
#     DIRECTORY "${CMAKE_SOURCE_DIR}/smtk/simulation/wavetank/operations/"
#     DESTINATION "${SIMULATION_WORKFLOWS_ROOT}/operations"
#   )
# endif ()

# if (NOT BUILD_DOCUMENTATION STREQUAL "never")
#   add_subdirectory(docs)
# endif()
