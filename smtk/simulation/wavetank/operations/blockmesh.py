# =============================================================================
#
#  Copyright (c) Kitware, Inc.
#  All rights reserved.
#  See LICENSE.txt for details.
#
#  This software is distributed WITHOUT ANY WARRANTY; without even
#  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#  PURPOSE.  See the above copyright notice for more information.
#
# =============================================================================
"""BlockMesh operation"""

import os
import shutil
import sys

import smtk
import smtk.attribute
import smtk.io
import smtk.operation
import smtk.project

OP_SUCCEEDED = int(smtk.operation.Operation.Outcome.SUCCEEDED)

# Make sure __file__ is set when using modelbuilder
import inspect
source_file = os.path.abspath(inspect.getfile(inspect.currentframe()))
__file__ = source_file

# Make sure this folder is in sys.path
source_dir = os.path.abspath(os.path.dirname(__file__))
if source_dir not in sys.path:
    sys.path.insert(0, source_dir)

from foam_mixin import FoamMixin
from control_dict_writer import ControlDictWriter
from process_status import ProcessStatus


class BlockMesh(smtk.operation.Operation, FoamMixin):
    """Configures and optionally runs blockMesh to generate background mesh.

    Generates controlDict and blockMeshDict files.
    Runs blockMesh either sync or async mode.
    Uses foam_operation.sbt for template.
    """

    def __init__(self):
        smtk.operation.Operation.__init__(self)
        FoamMixin.__init__(self)
        # Do NOT store any smtk resources as member data (causes memory leak)

    def name(self):
        return "Setup and run blockMesh"

    def operateInternal(self):
        """"""
        project = self._get_project()
        if project is None:
            return self.createResult(smtk.operation.Operation.Outcome.UNABLE_TO_OPERATE)

        att_resource = self._get_attribute_resource(project)
        # print('att_resource:', att_resource)
        if att_resource is None:
            return self.createResult(smtk.operation.Operation.Outcome.UNABLE_TO_OPERATE)

        # Check attributes
        att_names = ['controlDict', 'BackgroundGeometry', 'BlockMesh']
        if not self._check_attributes(att_resource, att_names):
            return self.createResult(smtk.operation.Operation.Outcome.UNABLE_TO_OPERATE)

       # Get case directory (based on OpenFOAM application)
        solver = self.cd_writer.get_application_name(att_resource)
        project_dir = os.path.abspath(os.path.dirname(project.location()))
        if solver == 'interFoam':
            case_dir = os.path.join(project_dir, 'foam/interFoam')
        else:
            self.log().addError('Unrecognized solver: {}'.format(solver))
            return self.createResult(smtk.operation.Operation.Outcome.UNABLE_TO_OPERATE)

        constant_dir = os.path.join(case_dir, 'constant')
        system_dir = os.path.join(case_dir, 'system')
        for path in [constant_dir, system_dir]:
            if not os.path.exists(path):
                os.makedirs(path)

        # Generate controlDict file
        if not self.cd_writer.generate_controlDict(case_dir, att_resource):
            return self.createResult(smtk.operation.Operation.Outcome.FAILED)

        workflows_dir = os.path.join(source_dir, *[os.pardir] * 4, 'simulation-workflows')
        foam_dir = os.path.join(workflows_dir, 'internal/foam/{}'.format(solver))

        from_path = os.path.join(foam_dir, 'system')
        shutil.copytree(from_path, system_dir, dirs_exist_ok=True)

        # Generate bgMeshing file
        if not self._generate_blockMeshConfig(case_dir, att_resource):
            return self.createResult(smtk.operation.Operation.Outcome.FAILED)

        # Check "Run" option
        run_item = self.parameters().findString('run')
        if run_item.isEnabled():
            if not self._run_openfoam('blockMesh', case_dir, run_item, foamfile='mesh.foam'):
                return self.createResult(smtk.operation.Operation.Outcome.FAILED)

        result = self._create_result()
        return result

    def createSpecification(self):
        spec = self._create_specification(app='blockMesh')
        return spec

    def _generate_blockMeshConfig(self, case_dir: str, att_resource: smtk.attribute.Resource) -> bool:
        """Writes constant/include/blockMeshConfig file. Returns True on success."""
        # Build (key,value) list
        kv_list = list()
        complete = False

        scale_att = att_resource.findAttribute('Scale')
        scale_item = scale_att.findDouble('scale')
        kv_list.append(('scale', scale_item.value()))

        geom_att = att_resource.findAttribute('BackgroundGeometry')

        box_item = geom_att.itemAtPath('box')
        names = ['Xmin', 'Xmax', 'Ymin', 'Ymax', 'Zmin', 'Zmax']
        for i, name in enumerate(names):
            kv_list.append((name, box_item.value(i)))

        mesh_att = att_resource.findAttribute('BlockMesh')
        size_item = mesh_att.findString('MeshSize')
        cell_counts = self._get_cell_counts(size_item, box_item)
        if cell_counts is None:
            return False
        names = ['xCells', 'yCells', 'zCells']
        for t in zip(names, cell_counts):
            kv_list.append(t)

        # Make sure directory exists
        include_dir = os.path.join(case_dir, 'constant/include')
        if not os.path.exists(include_dir):
            os.makedirs(include_dir)

        path = os.path.join(include_dir, 'bgMeshing')
        with open(path, 'w') as fp:
            self.cd_writer.write_kvlist(fp, kv_list, double_space=False)
            complete = True

        return complete

    def _get_cell_counts(self, meshsize_item: smtk.attribute.StringItem, box_item: smtk.attribute.DoubleItem) -> bool:
        """Parses options in MeshSize item."""
        # Each option has one child item, so find the active one
        numcells_item = meshsize_item.findChild(
            'numcells-eachdir', smtk.attribute.SearchStyle.IMMEDIATE_ACTIVE)
        if numcells_item is not None:
            cell_counts = [None] * 3
            for i in range(3):
                cell_counts[i] = numcells_item.value(i)
            return cell_counts

        # Remaining cases need the lengths of each direction and the max length
        lengths = [None] * 3
        lengths[0] = box_item.value(1) - box_item.value(0)
        lengths[1] = box_item.value(3) - box_item.value(2)
        lengths[2] = box_item.value(5) - box_item.value(4)
        max_length = max(lengths)

        # Determine target cell size
        numcells = [None] * 3
        cell_size = None

        maxdir_child = meshsize_item.findChild(
            'numcells-maxdir', smtk.attribute.SearchStyle.IMMEDIATE_ACTIVE)
        if maxdir_child is not None:
            cell_size = max_length / maxdir_child.value()

        relative_child = meshsize_item.findChild(
            'relative-meshsize', smtk.attribute.SearchStyle.IMMEDIATE_ACTIVE)
        if relative_child is not None:
            cell_size = max_length * relative_child.value()

        absolute_child = meshsize_item.findChild(
            'absolute-meshsize', smtk.attribute.SearchStyle.IMMEDIATE_ACTIVE)
        if absolute_child is not None:
            cell_size = absolute_child.value()

        if cell_size is None:
            self.log().addError('Error finding mesh size')
            return None

        for i in range(3):
            numcells[i] = int(lengths[i] / cell_size)

        return numcells
