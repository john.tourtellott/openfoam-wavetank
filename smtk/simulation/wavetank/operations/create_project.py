# =============================================================================
#
#  Copyright (c) Kitware, Inc.
#  All rights reserved.
#  See LICENSE.txt for details.
#
#  This software is distributed WITHOUT ANY WARRANTY; without even
#  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#  PURPOSE.  See the above copyright notice for more information.
#
# =============================================================================
"""CreateProject operation"""

import os
import shutil

import smtk
import smtk.attribute
import smtk.attribute_builder
import smtk.io
import smtk.operation
import smtk.project

OP_SUCCEEDED = int(smtk.operation.Operation.Outcome.SUCCEEDED)
PROJECT_NAME = 'foam.wavetank'

# Make sure __file__ is set when using modelbuilder
import inspect
source_file = os.path.abspath(inspect.getfile(inspect.currentframe()))
__file__ = source_file


class CreateProject(smtk.operation.Operation):
    """Creates project directory for running OpenFOAM simulation."""

    def __init__(self):
        super(CreateProject, self).__init__()

    def name(self):
        return "create wave tank project"

    def operateInternal(self):
        # Set up project directory
        folder = self.parameters().findDirectory('directory').value()
        if os.path.exists(folder) and len(os.listdir(folder)) > 0:
            overwrite = self.parameters().findVoid('overwrite').isEnabled()
            if overwrite:
                shutil.rmtree(folder)
            else:
                message = 'Cannot use folder with existing contents: {}'.format(folder)
                self.log().addError(message)
                raise RuntimeError(message)

        if not os.path.exists(folder):
            os.makedirs(folder)

        # Create smtk::project::Project instance
        # Must use Create operation because python ops cannot subclass smtk.project.Operation
        create_op = self.manager().createOperation('smtk::project::Create')
        create_op.parameters().findString('typeName').setValue(PROJECT_NAME)
        create_result = create_op.operate()
        create_outcome = create_result.findInt('outcome').value()
        if create_outcome != OP_SUCCEEDED:
            message = 'Create operation failed with outcome {}'.format(create_outcome)
            self.log().addError(message)
            return self.createResult(int(smtk.operation.Operation.Outcome.FAILED))
        project = create_result.findResource('resource').value()

        name = os.path.split(folder)[-1]
        project.setName(name)
        filename = '{}.project.smtk'.format(name)
        location = os.path.join(folder, filename)
        project.setLocation(location)

        # Locate workflows directory
        source_dir = os.path.abspath(os.path.dirname(__file__))
        workflows_dir = os.path.join(source_dir, *[os.pardir] * 4, 'simulation-workflows')

        # Create attribute resource
        sbt_path = os.path.join(workflows_dir, 'WaveTank.sbt')
        if not os.path.exists(sbt_path):
            message('Template file not found: {}'.format(sbt_path))
            self.log().addError(message)
            raise RuntimeError(message)

        sbt_op = self.manager().createOperation('smtk::attribute::Import')
        sbt_op.parameters().findFile('filename').setValue(sbt_path)
        sbt_result = sbt_op.operate()
        sbt_outcome = sbt_result.findInt('outcome').value()
        if sbt_outcome != OP_SUCCEEDED:
            message = 'Error importing attribute template file {}'.format(sbt_path)
            self.log().addError(message)
            return self.createResult(int(smtk.operation.Operation.Outcome.FAILED))
        att_res = sbt_result.findResource('resource').value()

        # Create instanced attributes
        builder = smtk.attribute_builder.AttributeBuilder()
        # builder.att_resource = att_res  # pending fix to smtk
        builder.create_instanced_atts(att_res)

        role = 'attributes'
        att_res.setName(role)
        filename = '{}.{}.smtk'.format(role, att_res.id())
        path = os.path.join(folder, 'resources', filename)
        att_res.setLocation(path)

        project.resources().add(att_res, role)

        # Create root case folder
        path = os.path.join(folder, 'foam')
        os.makedirs(path)

        this_result = self.createResult(smtk.operation.Operation.Outcome.SUCCEEDED)
        this_result.findResource('resource').setValue(project)
        return this_result

    def createSpecification(self):
        spec = self.createBaseSpecification()

        source_dir = os.path.dirname(__file__)
        sbt_path = os.path.join(source_dir, 'create_project.sbt')
        if not os.path.exists(sbt_path):
            message = 'File not found: {}'.format(sbt_path)
            self.log().addError(message)
            raise RuntimeError(message)

        reader = smtk.io.AttributeReader()
        hasErr = reader.read(spec, sbt_path, self.log())
        if hasErr:
            message = 'Error loading specification file: {}'.format(sbt_path)
            self.log().addError(message)
            raise RuntimeError(message)
        return spec
