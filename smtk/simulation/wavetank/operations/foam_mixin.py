# =============================================================================
#
#  Copyright (c) Kitware, Inc.
#  All rights reserved.
#  See LICENSE.txt for details.
#
#  This software is distributed WITHOUT ANY WARRANTY; without even
#  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#  PURPOSE.  See the above copyright notice for more information.
#
# =============================================================================
"""Common utilities for openfoam operations"""

import os
import subprocess
import sys

import smtk
import smtk.attribute
import smtk.io
import smtk.operation
import smtk.project


# Make sure __file__ is set when using modelbuilder
import inspect
source_file = os.path.abspath(inspect.getfile(inspect.currentframe()))
__file__ = source_file

from control_dict_writer import ControlDictWriter
from process_status import ProcessStatus


class FoamMixin:
    """Python MIXIN CLASS for operations that run OpenFoam applications.

    Provides a set of common methods used by many operations.
    Not implemented as an smtk.operation.Operation subclass because smtk cannot find
    operateInternal() method of its subclasses.
    """

    def __init__(self):
        # Do NOT store any smtk resources as member data (causes memory leak)
        self.source_dir = os.path.abspath(os.path.dirname(__file__))
        self.cd_writer = ControlDictWriter()  # for subclass

        # Operation results data
        self.status = 0
        self.pid = 0
        self.logfile = ''

    def _create_specification(self, app=None, sbt_file='foam_operation.sbt'):
        """Create default specification.

        Inputs
            app: if specified, update "Run" label to use app name
            sbt_file: template file to create specification
        """
        spec = self.createBaseSpecification()

        sbt_path = os.path.join(self.source_dir, sbt_file)
        if not os.path.exists(sbt_path):
            message = 'File not found: {}'.format(sbt_path)
            self.log().addError(message)
            raise RuntimeError(message)

        reader = smtk.io.AttributeReader()
        hasErr = reader.read(spec, sbt_path, self.log())
        if hasErr:
            message = 'Error loading specification file: {}'.format(sbt_path)
            self.log().addError(message)
            raise RuntimeError(message)

        # Update run label if specified
        if app is not None:
            att_defn = spec.findDefinition('setup')
            pos = att_defn.findItemPosition('run')
            if pos >= 0:
                run_defn = att_defn.itemDefinition(pos)
                label = 'Run {}'.format(app)
                run_defn.setLabel(label)
        return spec

    def _get_project(self) -> smtk.project.Project:
        """Gets project from parameter association, or returns None if not found."""
        err_message = None
        ref_item = self.parameters().associations()
        if ref_item is None:
            err_message = 'internal error: project association item is missing'
        elif ref_item.numberOfValues() != 1:
            err_message = 'should have 1 project association not {}'.ref_item.numberOfValues()
        if err_message:
            self.log().addError(err_message)
            return None

        project = ref_item.value()
        return project

    def _get_attribute_resource(self, project: smtk.project.Project) -> smtk.attribute.Resource:
        """Get project's attribute resource."""
        att_resource_set = project.resources().findByRole('attributes')
        if not att_resource_set:
            self.log().addError('Project missing attribute resource.')
            return None

        att_resource = att_resource_set.pop()
        return att_resource

    def _check_attributes(self, att_resource, att_names: list) -> bool:
        """Check instanced attributes validity"""
        ok = True  # return value
        for att_name in att_names:
            att = att_resource.findAttribute(att_name)
            if att is None:
                self.log().addError('Did not find attribute {}'.format(att_name))
            elif not att.isValid():
                self.log().addError('Attribute {} is not valid'.format(att_name))
        return ok

    def _run_openfoam(self,
                      app: str,
                      case_dir: str,
                      run_item: smtk.attribute.IntItem,
                      args_list: list = [],
                      foamfile: str = None) -> bool:
        """Runs openfoam application from case directory."""
        log_dir = os.path.join(case_dir, 'logs')
        if not os.path.exists(log_dir):
            os.makedirs(log_dir)
        self.logfile = os.path.join(log_dir, '{}.log'.format(app))
        errfile = os.path.join(log_dir, 'stderr.log')

        # Set PWD for standard (non-docker) OpenFOAM binaries
        env = os.environ.copy()
        env['PWD'] = case_dir

        args = [app] + args_list
        # Check hack for docker implementation
        if hasattr(smtk, 'use_openfoam_docker') and smtk.use_openfoam_docker:
            args = ['openfoam-docker', '/'] + args

        run_mode = run_item.value()
        if run_mode == 'sync':  # run and wait for completion
            with open(self.logfile, 'w') as fp:
                completed_proc = subprocess.run(
                    args, cwd=case_dir, env=env, universal_newlines=True)
                if completed_proc.returncode == 0:
                    self.status = ProcessStatus.Completed
                    success = True

                    if foamfile is not None:
                        if not foamfile.endswith('.foam'):
                            foamfile = '{}.foam'.format(foamfile)
                        foamfile_path = os.path.join(case_dir, foamfile)
                        with open(foamfile_path, 'a'):
                            os.utime(foamfile_path, None)

                else:
                    self.log().addError('{} returned code {}'.format(app, completed_proc.returncode))
                    self.status = ProcessStatus.Error
                    success = False

            return success

        elif run_mode == 'async':  # launch and return
            with open(self.logfile, 'w') as out, open(errfile, 'w') as err:
                proc = subprocess.Popen(
                    args, stdout=out, stderr=err, cwd=case_dir, env=env, universal_newlines=True)
                self.log().addRecord(smtk.io.Logger.Info, 'Started process {}'.format(proc.pid))
                self.status = ProcessStatus.Started
                self.pid = proc.pid
            return True

        else:
            self.log().addError('internal error - unrecognized run_mode {}'.format(run_mode))
            return False

    def _create_result(self):
        """Creates SUCCEEDED result and sets process-related items."""
        result = self.createResult(smtk.operation.Operation.Outcome.SUCCEEDED)
        result.findInt('status').setValue(self.status)
        result.findInt('pid').setValue(self.pid)
        result.findString('logfile').setValue(self.logfile)
        return result
