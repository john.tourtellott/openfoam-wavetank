<?xml version="1.0" encoding="utf-8" ?>
<SMTK_AttributeResource Version="5">
  <!-- WaveTank Project "snappyHexMesh" Operation for interfoam -->
  <!-- Only loads via snappyMesh.py file (which preloads base definitions) -->

  <Definitions>
    <AttDef Type="snappyhexmesh" Label="snappyHexMesh" BaseType="operation">
      <BriefDescription>
        Set up and run snappyHexMesh in the "analysisMesh" case
      </BriefDescription>
      <AssociationsDef Name="project" Label="Project" NumberOfRequiredValues="1"
                       LockType="Read" Extensible="false" OnlyResources="true">
        <Accepts>
          <Resource Name="smtk::project::Project"/>
        </Accepts>
      </AssociationsDef>

      <ItemDefinitions>
        <String Name="run" Label="Run snappyHexMesh" Optional="true" IsEnabledByDefault="true">
          <DiscreteInfo DefaultIndex="0">
            <Value Enum="Wait For Completion">sync</Value>
            <Value Enum="Launch And Return">async</Value>
          </DiscreteInfo>
        </String>

        <Void Name="TestMode" Label="Test Mode" Optional="true" IsEnabledByDefault="false" AdvanceLevel="1">
          <BriefDescription>For internal use</BriefDescription>
        </Void>
      </ItemDefinitions>
    </AttDef>

    <AttDef Type="result(snappyhexmesh)" BaseType="result">
      <ItemDefinitions>
        <Int Name="status"><DefaultValue>0</DefaultValue></Int>
        <Int Name="pid"><DefaultValue>0</DefaultValue></Int>
        <String Name="logfile"></String>
      </ItemDefinitions>
    </AttDef>
  </Definitions>
</SMTK_AttributeResource>
