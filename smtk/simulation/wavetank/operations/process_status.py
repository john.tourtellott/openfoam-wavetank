class ProcessStatus:
    """Status of OpenFoam application run"""
    NotRun = 0
    Started = 1
    Completed = 2
    Error = 9
