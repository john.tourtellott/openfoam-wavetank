# =============================================================================
#
#  Copyright (c) Kitware, Inc.
#  All rights reserved.
#  See LICENSE.txt for details.
#
#  This software is distributed WITHOUT ANY WARRANTY; without even
#  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#  PURPOSE.  See the above copyright notice for more information.
#
# =============================================================================
"""interFoam operation"""

import os
import sys

import smtk
import smtk.attribute
import smtk.io
import smtk.operation
import smtk.project

OP_SUCCEEDED = int(smtk.operation.Operation.Outcome.SUCCEEDED)

# Make sure __file__ is set when using modelbuilder
import inspect
source_file = os.path.abspath(inspect.getfile(inspect.currentframe()))
__file__ = source_file

# Make sure this folder is in sys.path
source_dir = os.path.dirname(__file__)
if source_dir not in sys.path:
    sys.path.insert(0, source_dir)

from foam_mixin import FoamMixin
from card_format import CardFormat
from control_dict_writer import ControlDictWriter
from process_status import ProcessStatus


class InterFoam(smtk.operation.Operation, FoamMixin):
    """Configures and optionally runs interFoam application.

    Uses foam_operation.sbt for template.
    """

    def __init__(self):
        smtk.operation.Operation.__init__(self)
        FoamMixin.__init__(self)
        # Do NOT store any smtk resources as member data (causes memory leak)

    def name(self):
        return "Setup and run interFoam"

    def operateInternal(self):
        """"""
        project = self._get_project()
        if project is None:
            return self.createResult(smtk.operation.Operation.Outcome.UNABLE_TO_OPERATE)

        att_resource = self._get_attribute_resource(project)
        # print('att_resource:', att_resource)
        if att_resource is None:
            return self.createResult(smtk.operation.Operation.Outcome.UNABLE_TO_OPERATE)

       # Get solver
        solver = self.cd_writer.get_application_name(att_resource)
        if solver != 'interFoam':
            self.log().addError('Unrecognized solver: {}'.format(solver))
            return self.createResult(smtk.operation.Operation.Outcome.UNABLE_TO_OPERATE)

        # Get project and case directories
        project_dir = os.path.abspath(os.path.dirname(project.location()))
        case_dir = os.path.join(project_dir, 'foam', solver)

        # Check if controlDict and solver attributes are valid
        att_names = ['controlDict', solver]
        if not self._check_attributes(att_resource, att_names):
            return self.createResult(smtk.operation.Operation.Outcome.UNABLE_TO_OPERATE)

        # (Re)generate controlDict file
        if not self.cd_writer.generate_controlDict(case_dir, att_resource):
            return self.createResult(smtk.operation.Operation.Outcome.FAILED)

        # Generate wavePropertiesConfig file
        self._generate_wavePropertiesConfig(case_dir, att_resource)

        # Check "Run" option
        run_item = self.parameters().findString('run')
        if run_item.isEnabled():
            if not self._run_openfoam('interFoam', case_dir, run_item, foamfile='inter'):
                return self.createResult(smtk.operation.Operation.Outcome.FAILED)

        result = self._create_result()
        return result

    def createSpecification(self):
        spec = self._create_specification(app='interFoam')
        return spec

    def _generate_wavePropertiesConfig(self, case_dir: str, att_resource: smtk.attribute.Resource) -> bool:
        """Writes constant/include/wavePropertiesConfig file. Returns True on success."""
        # Build (key,value) list
        kv_list = list()
        complete = False

        # Make sure directory exists
        include_dir = os.path.join(case_dir, 'constant/include')
        if not os.path.exists(include_dir):
            os.makedirs(include_dir)

        # Hard code to interFoam attribute for now
        solver_att = att_resource.findAttribute('interFoam')
        cards = [
            CardFormat(solver_att, ''),
            CardFormat(solver_att, 'waveHeight', item_path='Wave/WaveHeight'),
            CardFormat(solver_att, 'wavePeriod', item_path='Wave/WavePeriod'),
            CardFormat(solver_att, 'waveAngle', item_path='Wave/WaveAngle'),
            CardFormat(solver_att, 'rampTime', item_path='Wave/RampTime'),
        ]
        for card in cards:
            kv = card.to_string_tuple()
            if kv is not None:
                kv_list.append(kv)

        path = os.path.join(include_dir, 'wavePropertiesConfig')
        with open(path, 'w') as fp:
            self.cd_writer.write_kvlist(fp, kv_list, double_space=False)
            complete = True

        return complete
