# =============================================================================
#
#  Copyright (c) Kitware, Inc.
#  All rights reserved.
#  See LICENSE.txt for details.
#
#  This software is distributed WITHOUT ANY WARRANTY; without even
#  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#  PURPOSE.  See the above copyright notice for more information.
#
# =============================================================================
"""SetFields operation"""

import os
import shutil
import sys

import smtk
import smtk.attribute
import smtk.io
import smtk.operation
import smtk.project

OP_SUCCEEDED = int(smtk.operation.Operation.Outcome.SUCCEEDED)

# Make sure __file__ is set when using modelbuilder
import inspect
source_file = os.path.abspath(inspect.getfile(inspect.currentframe()))
__file__ = source_file

# Make sure this folder is in sys.path
source_dir = os.path.dirname(__file__)
if source_dir not in sys.path:
    sys.path.insert(0, source_dir)

from foam_mixin import FoamMixin
from control_dict_writer import ControlDictWriter
from process_status import ProcessStatus


class SetFields(smtk.operation.Operation, FoamMixin):
    """Configures and optionally runs setFields application.

    Uses foam_operation.sbt for template.
    """

    def __init__(self):
        smtk.operation.Operation.__init__(self)
        FoamMixin.__init__(self)
        # Do NOT store any smtk resources as member data (causes memory leak)

    def name(self):
        return "Setup and run setFields"

    def operateInternal(self):
        """"""
        project = self._get_project()
        if project is None:
            return self.createResult(smtk.operation.Operation.Outcome.UNABLE_TO_OPERATE)

        self.project_dir = os.path.abspath(os.path.dirname(project.location()))
        self.case_dir = os.path.join(self.project_dir, 'foam/interFoam')

        att_resource = self._get_attribute_resource(project)
        # print('att_resource:', att_resource)
        if att_resource is None:
            return self.createResult(smtk.operation.Operation.Outcome.UNABLE_TO_OPERATE)

        # Get case directory (based on OpenFOAM application)
        solver = self.cd_writer.get_application_name(att_resource)
        project_dir = os.path.abspath(os.path.dirname(project.location()))
        if solver == 'interFoam':
            case_dir = os.path.join(project_dir, 'foam/interFoam')
        else:
            self.log().addError('Unrecognized solver: {}'.format(solver))
            return self.createResult(smtk.operation.Operation.Outcome.UNABLE_TO_OPERATE)

        # (Re)generate controlDict file
        # Dont think this is needed for setFields
        # if not self.cd_writer.generate_controlDict(case_dir, att_resource):
        #     return self.createResult(smtk.operation.Operation.Outcome.FAILED)

        # Copy 0.orig and constant folders
        root_dir = os.path.abspath(os.path.join(source_dir, *[os.pardir] * 4))
        solver_path = 'simulation-workflows/internal/foam/{}'.format(solver)
        dirnames = ['0.orig', 'constant']
        for dirname in dirnames:
            from_dir = os.path.join(root_dir, solver_path, dirname)
            to_dir = os.path.join(case_dir, dirname)
            shutil.copytree(from_dir, to_dir, dirs_exist_ok=True)

            # Also copy 0.orig to 0
            if dirname == '0.orig':
                to_dir = os.path.join(case_dir, '0')
                if os.path.exists(to_dir):
                    shutil.rmtree(to_dir)
                shutil.copytree(from_dir, to_dir)

        # Generate setFieldsConfig file
        if not self._generate_setFieldsConfig(case_dir, att_resource):
            return self.createResult(smtk.operation.Operation.Outcome.FAILED)

        run_item = self.parameters().findString('run')
        if run_item.isEnabled():
            if not self._run_openfoam('setFields', case_dir, run_item):
                return self.createResult(smtk.operation.Operation.Outcome.FAILED)

        result = self._create_result()
        return result

    def createSpecification(self):
        spec = self._create_specification(app='setFields')
        return spec

    def _generate_setFieldsConfig(self, case_dir: str, att_resource: smtk.attribute.Resource) -> bool:
        """Writes constant/include/setFieldsConfig file. Returns True on success."""
        # Build (key,value) list
        kv_list = list()
        complete = False

        # Make sure directory exists
        include_dir = os.path.join(case_dir, 'constant/include')
        if not os.path.exists(include_dir):
            os.makedirs(include_dir)

        # Hard code to interFoam attribute for now
        solver_att = att_resource.findAttribute('interFoam')
        surface_item = solver_att.findDouble('WaterSurface')
        t = ('waterSurface', surface_item.value())
        kv_list.append(t)

        path = os.path.join(include_dir, 'setFieldsConfig')
        with open(path, 'w') as fp:
            self.cd_writer.write_kvlist(fp, kv_list, double_space=False)
            complete = True

        return complete
