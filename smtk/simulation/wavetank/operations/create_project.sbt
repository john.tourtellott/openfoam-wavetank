<?xml version="1.0" encoding="utf-8" ?>
<SMTK_AttributeResource Version="5">
  <!-- Description of the WaveTank Project "Create" Operation -->
  <!-- Only loads via create_project.py file (which preloads base definitions) -->

  <Definitions>
    <AttDef Type="create" Label="Project - Create" BaseType="operation">
      <BriefDescription>
        Create a CMB project for OpenFOAM wave tank simulation.
      </BriefDescription>

      <ItemDefinitions>
        <Directory Name="directory" Label="Project Directory" ShouldExist="true" FileFilters="All Dirs (*)">
          <BriefDescription>The folder in the local filesystem for storing project files.</BriefDescription>
        </Directory>
        <Void Name="overwrite" Label="OK to overwrite existing directory?" Optional="true" IsEnabledByDefault="false">
          <BriefDescription>Flag indicating whether or not it OK to erase the contents of project folder if it is not empty.</BriefDescription>
        </Void>
      </ItemDefinitions>
    </AttDef>

    <AttDef Type="result(create)" BaseType="result">
      <ItemDefinitions>
        <Resource Name="resource" HoldReference="true">
          <Accepts>
            <Resource Name="smtk::project::Project"/>
          </Accepts>
        </Resource>
      </ItemDefinitions>
    </AttDef>
  </Definitions>
</SMTK_AttributeResource>
