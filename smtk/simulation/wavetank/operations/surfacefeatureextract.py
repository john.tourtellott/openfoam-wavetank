# =============================================================================
#
#  Copyright (c) Kitware, Inc.
#  All rights reserved.
#  See LICENSE.txt for details.
#
#  This software is distributed WITHOUT ANY WARRANTY; without even
#  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#  PURPOSE.  See the above copyright notice for more information.
#
# =============================================================================
"""SurfaceFeatureExtract operation"""

import os
import shutil
import sys

import smtk
import smtk.attribute
import smtk.io
import smtk.mesh
import smtk.operation
import smtk.project
import smtk.session.mesh

OP_SUCCEEDED = int(smtk.operation.Operation.Outcome.SUCCEEDED)

# Make sure __file__ is set when using modelbuilder
import inspect
source_file = os.path.abspath(inspect.getfile(inspect.currentframe()))
__file__ = source_file

# Make sure this folder is in sys.path
source_dir = os.path.abspath(os.path.dirname(__file__))
if source_dir not in sys.path:
    sys.path.insert(0, source_dir)

from foam_mixin import FoamMixin
from control_dict_writer import ControlDictWriter
from process_status import ProcessStatus


class SurfaceFeatureExtract(smtk.operation.Operation, FoamMixin):
    """Setup and run surfaceFeaturesExtract"""

    def __init__(self):
        smtk.operation.Operation.__init__(self)
        FoamMixin.__init__(self)
        # Do NOT store any smtk resources as member data (causes memory leak)

    def name(self):
        return "Setup and run surfaceFeaturesExtract"

    def createSpecification(self):
        spec = self._create_specification(app='surfaceFeatureExtract')
        return spec

    def operateInternal(self):
        """"""
        project = self._get_project()
        if project is None:
            return self.createResult(smtk.operation.Operation.Outcome.UNABLE_TO_OPERATE)

        att_resource = self._get_attribute_resource(project)
        if att_resource is None:
            return self.createResult(smtk.operation.Operation.Outcome.UNABLE_TO_OPERATE)

        # Check 'Object' attribute
        att_names = ['Object']
        if not self._check_attributes(att_resource, att_names):
            return self.createResult(smtk.operation.Operation.Outcome.UNABLE_TO_OPERATE)

        # Also check that Object attribute is assigned a geometry file ("Filename" item)
        object_att = att_resource.findAttribute('Object')
        object_filename_item = object_att.findString('Filename')
        if not object_filename_item.isEnabled():
            self.log().addError('No model file to input to surfaceFeatureExtract')
            return self.createResult(smtk.operation.Operation.Outcome.UNABLE_TO_OPERATE)

       # Get case directory (based on OpenFOAM application)
        solver = self.cd_writer.get_application_name(att_resource)
        project_dir = os.path.abspath(os.path.dirname(project.location()))
        if solver == 'interFoam':
            case_dir = os.path.join(project_dir, 'foam/interFoam')
        else:
            self.log().addError('Unrecognized solver: {}'.format(solver))
            return self.createResult(smtk.operation.Operation.Outcome.UNABLE_TO_OPERATE)

        # Make sure constant and system directories are created
        constant_dir = os.path.join(case_dir, 'constant')
        system_dir = os.path.join(case_dir, 'system')
        for path in [constant_dir, system_dir]:
            if not os.path.exists(path):
                os.makedirs(path)

        # Make sure there is a controlDict file
        if not self.cd_writer.generate_controlDict(case_dir, att_resource):
            return self.createResult(smtk.operation.Operation.Outcome.FAILED)

        # Todo read-transform-write the native model file

        object_filename = object_filename_item.value()
        object_path = os.path.join(project_dir, 'assets', object_filename)
        geom_dir = os.path.join(constant_dir, 'triSurface')
        if not os.path.exists(geom_dir):
            os.makedirs(geom_dir)
        shutil.copy(object_path, geom_dir)

        if not self._write_surfaceFeatureExtractDict(object_att, system_dir, object_filename):
            return self.createResult(smtk.operation.Operation.Outcome.FAILED)

        # Check "Run" option
        run_item = self.parameters().findString('run')
        if run_item.isEnabled():
            if not self._run_openfoam('surfaceFeatureExtract', case_dir, run_item):
                return self.createResult(smtk.operation.Operation.Outcome.FAILED)

        result = self._create_result()
        return result

    def _write_surfaceFeatureExtractDict(self, object_att, directory, object_filename):
        """"""
        # Generate feature list
        kv_list = list()
        kv = ('extractionMethod', 'extractFromSurface')
        kv_list.append(kv)

        angle = None
        angle_item = object_att.itemAtPath('Edges')
        option = angle_item.value()
        if option == 'none':
            angle = 0
        elif option == 'all':
            angle = 180
        else:
            value_item = angle_item.findChild('Angle', smtk.attribute.SearchStyle.IMMEDIATE_ACTIVE)
            angle = value_item.value()
        kv = ('includedAngle', angle)
        kv_list.append(kv)

        tolerance = 0.001  # default
        tolerance_item = object_att.itemAtPath('Tolerance')
        if tolerance_item is not None:
            tolerance = tolerance_item.value()
        kv = ('tolerance', tolerance)
        kv_list.append(kv)

        dict_filename = 'surfaceFeatureExtractDict'
        path = os.path.join(directory, dict_filename)
        complete = False
        with open(path, 'w') as fp:
            self.cd_writer.write_foamfile_header(fp, dict_filename)
            fp.write('\n')

            # Write native model section
            fp.write('{}\n'.format(object_filename))
            fp.write('{\n')
            self.cd_writer.write_kvlist(fp, kv_list, indent=4, column0=20)
            fp.write('}\n')
            complete = True

        return complete
