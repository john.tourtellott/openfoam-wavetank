<?xml version="1.0" encoding="utf-8" ?>
<SMTK_AttributeResource Version="5" DisplayHint="true">
  <!-- ********** Background Mesh Specification ********** -->

  <Categories>
    <Cat>interfoam</Cat>
  </Categories>

  <Definitions>
    <AttDef Type="interFoam">
      <Categories><Cat>interfoam</Cat></Categories>
      <ItemDefinitions>
        <Double Name="WaterSurface" Label="Water surface">
          <BriefDescription>Initial height of the water</BriefDescription>
          <DefaultValue>0.0</DefaultValue>
        </Double>

        <Group Name="Wave" Label="Wave specification">
          <ItemDefinitions>
            <Double Name="WaveHeight" Label="Wave height">
              <DefaultValue>0.5</DefaultValue>
            </Double>

            <Double Name="WavePeriod" Label="Wave period">
              <DefaultValue>1.41</DefaultValue>
            </Double>

            <Double Name="WaveAngle" Label="Wave angle">
              <DefaultValue>0.0</DefaultValue>
            </Double>

            <Double Name="RampTime" Label="Ramp time">
              <DefaultValue>0.1</DefaultValue>
            </Double>
          </ItemDefinitions>
        </Group>

      </ItemDefinitions>
    </AttDef>
  </Definitions>
  </SMTK_AttributeResource>
