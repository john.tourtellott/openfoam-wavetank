<?xml version="1.0" encoding="utf-8" ?>
<SMTK_AttributeResource Version="5" DisplayHint="true">
  <!-- ********** Background Mesh Specification ********** -->

  <Categories>
    <Cat>background</Cat>
  </Categories>

  <Definitions>
    <AttDef Type="RefinementDirection" Label="Directions">
      <Categories><Cat>background</Cat></Categories>
      <ItemDefinitions>
        <Void Name="x" Label="X" Optional="true" IsEnabledByDefault="true" />
        <Void Name="y" Label="Y" Optional="true" IsEnabledByDefault="false" />
        <Void Name="z" Label="Z" Optional="true" IsEnabledByDefault="true" />
      </ItemDefinitions>
    </AttDef>

    <AttDef Type="RefinementRegion" Label="Region">
      <Categories><Cat>background</Cat></Categories>
      <ItemDefinitions>
        <Double Name="box" Label="Box" NumberOfRequiredValues="6" >
          <!-- Values are xmin, xmax, ymin, ymax, zmin, zmax -->
          <DefaultValue>-18,18,-6,6,-1,1</DefaultValue>
        </Double>
      </ItemDefinitions>
    </AttDef>
  </Definitions>
</SMTK_AttributeResource>
