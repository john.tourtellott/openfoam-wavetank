<?xml version="1.0" encoding="utf-8" ?>
<SMTK_AttributeResource Version="5" DisplayHint="true">
  <!-- ********** Background Mesh Specification ********** -->

  <Categories>
    <Cat>background</Cat>
  </Categories>

  <Definitions>
    <AttDef Type="Scale">
      <Categories><Cat>background</Cat></Categories>
      <ItemDefinitions>
        <Double Name="scale">
          <BriefDescription>Length of 1 coordinate unit in meters.</BriefDescription>
          <DefaultValue>1.0</DefaultValue>
        </Double>
      </ItemDefinitions>
    </AttDef>

    <AttDef Type="BoxWidget">
      <Categories><Cat>background</Cat></Categories>
      <ItemDefinitions>
        <Double Name="box" Label="Geometry" NumberOfRequiredValues="6" >
          <!-- Values are xmin, xmax, ymin, ymax, zmin, zmax -->
          <DefaultValue>-15,15,-4,4,-4,4</DefaultValue>
        </Double>
      </ItemDefinitions>
    </AttDef>

    <AttDef Type="BlockMesh">
      <Categories><Cat>background</Cat></Categories>
      <ItemDefinitions>
        <String Name="MeshSize" Label="Mesh Size">
          <ChildrenDefinitions>
            <Int Name="numcells-maxdir" Label=" ">
              <DefaultValue>100</DefaultValue>
              <RangeInfo><Min Inclusive="true">1</Min></RangeInfo>
            </Int>
            <Double Name="relative-meshsize" Label=" ">
              <DefaultValue>0.01</DefaultValue>
              <RangeInfo>
                <Min Inclusive="false">0.0</Min>
                <Max Inclusive="false">1.0</Max>
              </RangeInfo>
            </Double>
            <Double Name="absolute-meshsize" Label=" ">
              <DefaultValue>0.05</DefaultValue>
              <RangeInfo><Min Inclusive="false">0.0</Min></RangeInfo>
            </Double>
            <Int Name="numcells-eachdir" Label=" " NumberOfRequiredValues="3">
              <DefaultValue>100,20,20</DefaultValue>
              <RangeInfo><Min Inclusive="true">1</Min></RangeInfo>
            </Int>
          </ChildrenDefinitions>
          <DiscreteInfo DefaultIndex="0">
            <Structure>
              <Value Enum="Number of cells in longest direction">maxlen</Value>
              <Items><Item>numcells-maxdir</Item></Items>
            </Structure>
            <Structure>
              <Value Enum="Relative mesh size">relative-size</Value>
              <Items><Item>relative-meshsize</Item></Items>
            </Structure>
            <Structure>
              <Value Enum="Absolue mesh size">absolute-size</Value>
              <Items><Item>absolute-meshsize</Item></Items>
            </Structure>
            <Structure>
              <Value Enum="Number of cells in each direction">numcells</Value>
              <Items><Item>numcells-eachdir</Item></Items>
            </Structure>
          </DiscreteInfo>
        </String>
      </ItemDefinitions>
    </AttDef>

  </Definitions>
</SMTK_AttributeResource>
