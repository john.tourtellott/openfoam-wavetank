<?xml version="1.0" encoding="utf-8" ?>
<SMTK_AttributeResource Version="5" DisplayHint="true">
  <!-- ********** Background Mesh Specification ********** -->

  <Categories>
    <Cat>background</Cat>
  </Categories>

  <Definitions>
    <AttDef Type="SnappyRefinementRegion">
      <Categories><Cat>background</Cat></Categories>
      <ItemDefinitions>
        <Double Name="box" Label="Box" NumberOfRequiredValues="6" >
          <!-- Values are xmin, xmax, ymin, ymax, zmin, zmax -->
          <DefaultValue>-1,1,0.4,0.6,-0.1,0.1</DefaultValue>
        </Double>
        <String Name="mode">
          <ChildrenDefinitions>
            <Int Name="level" Label="Refinement Level">
              <BriefDescription>The desired refinement level.</BriefDescription>
              <DefaultValue>2</DefaultValue>
              <RangeInfo><Min Inclusive="true">1</Min></RangeInfo>
            </Int>
            <Group Name="levelIncrement" Label="Directional Refinement" Optional="true" IsEnabledByDefault="false">
              <ItemDefinitions>
                <Int Name="range" Label="Levels" NumberOfRequiredValues="2">
                  <ComponentLabels>
                    <Label>min:</Label>
                    <Label>max:</Label>
                  </ComponentLabels>
                  <DefaultValue>2,3</DefaultValue>
                  <RangeInfo><Min Inclusive="true">0</Min></RangeInfo>
                </Int>
                <Int Name="split" Label="Splits" NumberOfRequiredValues="3">
                  <ComponentLabels>
                    <Label>x:</Label>
                    <Label>y:</Label>
                    <Label>z:</Label>
                  </ComponentLabels>
                  <DefaultValue>1,0,0</DefaultValue>
                  <RangeInfo><Min Inclusive="true">1</Min></RangeInfo>
                </Int>
              </ItemDefinitions>
            </Group>
            <Group Name="distance-levels" Label="Distance &amp; Level" Extensible="true" NumberOfRequiredGroups="1">
              <ItemDefinitions>
                <Double Name="distance" Label="Distance">
                  <DefaultValue>1.0</DefaultValue>
                  <RangeInfo><Min Inclusive="false">0.0</Min></RangeInfo>
                </Double>
                <Int Name="level" Label="Refinement Level">
                  <BriefDescription>The desired refinement level.</BriefDescription>
                  <DefaultValue>2</DefaultValue>
                  <RangeInfo><Min Inclusive="true">1</Min></RangeInfo>
                </Int>
              </ItemDefinitions>
            </Group>
          </ChildrenDefinitions>
          <DiscreteInfo DefaultIndex="0">
            <Structure>
              <Value>inside</Value>
              <Items>
                <Item>level</Item>
                <Item>levelIncrement</Item>
              </Items>
            </Structure>
            <Structure>
              <Value>outside</Value>
              <Items>
                <Item>level</Item>
                <Item>levelIncrement</Item>
              </Items>
            </Structure>
            <Structure>
              <Value>distance</Value>
              <Items><Item>distance-levels</Item></Items>
            </Structure>
          </DiscreteInfo>
        </String>
      </ItemDefinitions>
    </AttDef>
  </Definitions>
</SMTK_AttributeResource>
