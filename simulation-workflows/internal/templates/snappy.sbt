<?xml version="1.0"?>
<SMTK_AttributeResource Version="5">

  <Categories>
    <Cat>object</Cat>
  </Categories>

  <Definitions>
    <AttDef Type="Castellation">
      <Categories>
        <Cat>object</Cat>
      </Categories>
      <ItemDefinitions>
        <Group Name="Castellation" Optional="true" IsEnabledByDefault="true">
          <ItemDefinitions>

            <Double Name="InsidePoint" Label="Inside Point" NumberOfRequiredValues="3">
              <BriefDescription>Specify point inside the background mesh (but *not* inside the object geometry).</BriefDescription>
              <DefaultValue>-5.0,0.2,0.0</DefaultValue>
            </Double>

            <!-- Todo maxLocalCells (for parallel meshing) -->

            <Int Name="maxGlobalCells">
              <BriefDescription>Overall cell limit (approximately).</BriefDescription>
              <DetailedDescription>Refinement will stop immediately
  upon reaching this number so a refinement level might not complete.
  Note that this is the number of cells before removing the part which
  is not 'visible' from the keepPoint. The final number of cells might
  actually be a lot less.</DetailedDescription>
              <DefaultValue>2000000</DefaultValue>
              <RangeInfo><Min Include="false">0</Min></RangeInfo>
            </Int>

            <Int Name="minRefinementCells">
              <BriefDescription>Stop refinement if number of cells is below this threshold.</BriefDescription>
              <DetailedDescription>The surface refinement loop might spend lots of iterations refining just a
  few cells. This setting will cause refinement to stop if &lt;= minimumRefinementCells
  are selected for refinement. Note: it will at least do one iteration
  (unless the number of cells to refine is 0)</DetailedDescription>
              <DefaultValue>10</DefaultValue>
              <RangeInfo><Min Include="true">1</Min></RangeInfo>
            </Int>

            <!-- Todo maxLoadUnbalance (for parallel meshing) -->

            <Int Name="nCellsBetweenLevels">
              <BriefDescription>Number of buffer layers between different levels.</BriefDescription>
              <DetailedDescription>1 means normal 2:1 refinement restriction, larger means slower
  refinement.</DetailedDescription>
              <DefaultValue>3</DefaultValue>
              <RangeInfo><Min Include="true">1</Min></RangeInfo>
            </Int>

            <Int Name="EdgeRefinementLevel" Label="Edge Refinement Level">
              <BriefDescription>Specifies a level for any cell intersected by its edges.</BriefDescription>
              <DetailedDescription>This is a featureEdgeMesh, read from constant/triSurface for now.</DetailedDescription>
              <DefaultValue>2</DefaultValue>
              <RangeInfo><Min Include="true">1</Min></RangeInfo>
            </Int>

            <Int Name="SurfaceRefinementLevels" Label="Surface Refinement Levels" NumberOfRequiredValues="2">
              <BriefDescription>Specifies min/max level for cells intersecting a surface.</BriefDescription>
              <ComponentLabels>
                <Label>Min:</Label>
                <Label>Max:</Label>
              </ComponentLabels>
              <DetailedDescription>Specifies two levels for every surface. The first is the minimum level,
  every cell intersecting a surface gets refined up to the minimum level.
  The second level is the maximum level. Cells that 'see' multiple
  intersections where the intersections make an
  angle &gt; resolveFeatureAngle get refined up to the maximum level.</DetailedDescription>
              <DefaultValue>2, 3</DefaultValue>
              <RangeInfo><Min Include="true">1</Min></RangeInfo>
            </Int>

            <Double Name="resolveFeatureAngle">
              <BriefDescription>Region-wise refinement</BriefDescription>
              <DefaultValue>30.0</DefaultValue>
              <RangeInfo>
                <Min Include="false">0.0</Min>
                <Max Include="false">180.0</Max>
              </RangeInfo>
            </Double>

            <!-- Todo refinementRegions (once I understand it) -->

            <!-- Todo allowFreeStandingZoneFaces (is it relevant?) -->
          </ItemDefinitions>
        </Group>
      </ItemDefinitions>
    </AttDef>

    <AttDef Type="SnapControls" Label="Snap Controls">
      <Categories>
        <Cat>object</Cat>
      </Categories>
      <ItemDefinitions>
        <Group Name="SnapControls" Label="Snapping" Optional="true" IsEnabledByDefault="true">
          <ItemDefinitions>

            <Int Name="nSmoothPatch">
              <BriefDescription>Number of patch smoothing iterations before finding correspondence to surface</BriefDescription>
              <DefaultValue>3</DefaultValue>
              <RangeInfo>
                <Min Include="true">0</Min>
              </RangeInfo>
            </Int>

            <Double Name="tolerance">
              <BriefDescription>Relative distance for points to be attracted by surface feature point or edge.</BriefDescription>
              <DetailedDescription>True distance is this factor times local maximum edge length.</DetailedDescription>
              <DefaultValue>2.0</DefaultValue>
              <RangeInfo>
                <Min Include="false">0.0</Min>
              </RangeInfo>
            </Double>

            <Int Name="nSolveIter">
              <BriefDescription>Number of mesh displacement relaxation iterations.</BriefDescription>
              <DefaultValue>30</DefaultValue>
              <RangeInfo>
                <Min Include="true">0</Min>
              </RangeInfo>
            </Int>

            <Int Name="nRelaxIter">
              <BriefDescription>Maximum number of snapping relaxation iterations.</BriefDescription>
              <DetailedDescription>Should stop before upon reaching a correct mesh.</DetailedDescription>
              <DefaultValue>5</DefaultValue>
              <RangeInfo>
                <Min Include="true">0</Min>
              </RangeInfo>
            </Int>

            <Group Name="FeatureSnapping" Label="Feature Snapping">
              <ItemDefinitions>
                <Int Name="nFeatureSnapIter" Optional="true" IsEnabledByDefault="true">
                  <BriefDescription>Number of feature edge snapping iterations.</BriefDescription>
                  <DefaultValue>10</DefaultValue>
                  <RangeInfo>
                    <Min Include="true">1</Min>
                  </RangeInfo>
                </Int>

                <Void Name="implicitFeatureSnap" Optional="true" IsEnabledByDefault="false">
                  <BriefDescription>Detect (geometric only) features by sampling the surface</BriefDescription>
                </Void>

                <Void Name="explicitFeatureSnap" Optional="true" IsEnabledByDefault="true">
                  <BriefDescription>Use Edge Refinement Level from Castellation.</BriefDescription>
                </Void>

                <Void Name="multiRegionFeatureSnap" Optional="true" IsEnabledByDefault="false">
                  <BriefDescription>Detect points on multiple surfaces (only for explicitFeatureSnap)</BriefDescription>
                </Void>
              </ItemDefinitions>
            </Group>

          </ItemDefinitions>
        </Group>
      </ItemDefinitions>
    </AttDef>

    <AttDef Type="MeshQualityControls">
      <Categories>
        <Cat>object</Cat>
      </Categories>
      <ItemDefinitions>
        <Double Name="minFaceWeight">
          <DefaultValue>0.02</DefaultValue>
          <RangeInfo>
            <Min Include="false">0.0</Min>
            <Max Include="true">0.5</Max>
          </RangeInfo>
        </Double>
      </ItemDefinitions>
    </AttDef>

  </Definitions>
</SMTK_AttributeResource>
