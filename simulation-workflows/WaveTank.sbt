<?xml version="1.0"?>
<SMTK_AttributeResource Version="5">

  <Categories>
    <Cat>interfoam</Cat>
    <Cat>background</Cat>
    <Cat>object</Cat>
  </Categories>

  <!-- For now, make all categories active -->
  <Analyses>
    <Analysis Type="interFoam">
      <Cat>interfoam</Cat>
      <Cat>background</Cat>
      <Cat>object</Cat>
    </Analysis>
  </Analyses>

  <Includes>
    <File>internal/templates/controlDict.sbt</File>
    <File>internal/templates/background.sbt</File>
    <File>internal/templates/interfoam.sbt</File>
    <File>internal/templates/snappy.sbt</File>
    <File>internal/templates/refinemesh.sbt</File>
    <File>internal/templates/object.sbt</File>
  </Includes>

  <Definitions>
    <!-- Explicity specify analysis attribute definition so that we can
         initialize it the way we want.

         Note that group items are marked AdvanceLevel 1 so that they do not
         appear in the standard UI. The contents of these groups are intended
         to be modified internally by the application software.
    -->
    <AttDef Type="Analysis">
      <ItemDefinitions>
        <String Name="Analysis" Label="Application" NumberOfRequiredValues="1">
          <ChildrenDefinitions>
            <Group Name="interFoam" NumberOfRequiredGroups="1" AdvanceLevel="1">
            </Group>
          </ChildrenDefinitions>
          <DiscreteInfo DefaultIndex="0">
            <Value>interFoam</Value>
          </DiscreteInfo>
        </String>
      </ItemDefinitions>
    </AttDef>
  </Definitions>

  <!--********** Views **********-->
  <Views>
    <View Type="Group" Title="Wave Tank" TopLevel="true" TabPosition="North"
    FilterByAdvanceLevel="true" FilterByCategory="false">
      <Views>
        <View Title="Control" />
        <View Title="Background" />
        <View Title="refineMesh" />
        <View Title="Object" />
        <View Title="Snappy" />
        <View Title="InterFoam" />
      </Views>
    </View>

    <View Type="Analysis" Title="Solver" Label="Solver"
      AnalysisAttributeName="Analysis" AnalysisAttributeType="Analysis">
    </View>

    <View Type="Group" Title="Control" Label="controlDict" Style="Tiled">
      <Views>
        <View Title="Solver" />
        <View Title="ControlDict" />
      </Views>
    </View>

    <View Type="Instanced" Title="ControlDict" Label="controlDict">
      <InstancedAttributes>
        <Att Type="controlDict" Name="controlDict" />
      </InstancedAttributes>
    </View>

    <View Type="Instanced" Title="Background" Label="blockMesh">
      <InstancedAttributes>
        <Att Type="Scale" Name="Scale" />
        <Att Type="BoxWidget" Name="BackgroundGeometry">
          <ItemViews>
            <View Item="box" Type="Box" Min="point 0" Max="point 1" Angles="angles" ShowControls="true"/>
          </ItemViews>
        </Att>
        <Att Type="BlockMesh" Name="BlockMesh" />
      </InstancedAttributes>
    </View>

    <View Type="Group" Title="refineMesh" Style="Tiled">
      <Views>
        <View Title="RefinementDirection" />
        <View Title="RefinementRegions" />
      </Views>
    </View>

    <View Type="Instanced" Title="RefinementDirection" Label="Refinement Directions">
      <InstancedAttributes>
        <Att Type="RefinementDirection" Name="RefinementDirection" />
      </InstancedAttributes>
    </View>

    <View Type="Attribute" Title="RefinementRegions" DisplaySearchBox="false">
      <AttributeTypes>
        <Att Type="RefinementRegion">
          <ItemViews>
            <View Item="box" Type="Box" Min="point 0" Max="point 1" Angles="angles" ShowControls="true"/>
          </ItemViews>
        </Att>
      </AttributeTypes>
    </View>

    <View Type="Instanced" Title="Object">
      <InstancedAttributes>
        <Att Type="Object" Name="Object">
          <ItemViews>
            <View Path="/InsidePoint" Type="Point" ShowControls="true" />
          </ItemViews>
        </Att>
      </InstancedAttributes>
    </View>

    <View Type="Group" Title="Snappy" Label="snappyHexMesh" TabPosition="North">
      <Views>
        <View Title="Castellation" />
        <View Title="Snapping" />
      </Views>
    </View>

    <View Type="Instanced" Title="Castellation">
      <InstancedAttributes>
        <Att Type="Castellation" Name="Castellation">
          <ItemViews>
            <View Path="/Castellation/InsidePoint" Type="Point" ShowControls="true" />
          </ItemViews>
        </Att>
      </InstancedAttributes>
    </View>

    <View Type="Instanced" Title="Snapping" >
      <InstancedAttributes>
        <Att Type="SnapControls" Name="SnapControls" />
      </InstancedAttributes>
    </View>

    <!-- <Att Type="MeshQualityControls" Name="MeshQualityControls" /> -->

    <View Type="Instanced" Title="InterFoam" Label="interFoam">
      <InstancedAttributes>
        <Att Type="interFoam" Name="interFoam" />
      </InstancedAttributes>
    </View>

  </Views>

</SMTK_AttributeResource>
