#!/usr/bin/env bash

# Run this from the "foam/interFoam" directory

# Exit when any command fails
set -e

echo Erase current mesh
rm -rf constant/polyMesh

echo Run blockMesh
openfoam-docker / blockMesh | tee blockMesh.log
touch inter.foam

echo Run snappyHexMesh
openfoam-docker / snappyHexMesh -overwrite | tee snappyHexMesh.log

echo Run setFields
openfoam-docker / setFields | tee setFields.log

echo Run interFoam
openfoam-docker / interFoam | tee interFoam.log
