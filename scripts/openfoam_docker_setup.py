"""This is a HACK for developer convenience.

This script runs the modelbuilder_setup.py script then adds a boolean
variable `use_openfoam_docker` to the smtk module. This is used to
support docker and native versions of OpenFOAM during initial
development.

Once a plugin is implemented, this HACK should be replaced with a
paraview setting.

Run this AFTER running modelbuilder_setup.py
"""
import smtk
smtk.use_openfoam_docker = True
print('smtk.use_openfoam_docker:', smtk.use_openfoam_docker)
