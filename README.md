Introduction
============
OpenFOAM Wave Tank is a plugin to work with [Computational Model Builder (CMB)][CMB],
specifically, the CMB ModelBuilder application. The plugin provides
simulation workflow files and operations for generating OpenFOAM input files
for wave tank analysis, in particular, for wave energy conversion (WEC)
devices.

See [GettingStarted.md][] for instructions on using the contents of this
project with the CMB modelbuilder application.

CMB is an open-source, multi-platform simulation workflow framework based on
[ParaView][], [Visualization Toolkit (VTK)][VTK] and [Qt][].

[CMB]: https://www.ComputationalModelBuilder.org
[ParaView]: https://www.paraview.org
[QT]: https://www.qt.io
[VTK]: http://www.vtk.org
[Kitware]: https://www.kitware.com

License
=======

CMB and this plugin are distributed under the OSI-approved BSD 3-clause
License. See [License.txt][] for details.

[License.txt]: LICENSE.txt
