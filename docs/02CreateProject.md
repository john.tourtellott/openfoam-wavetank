Creating a Wave Tank Project
=============================

Be sure to load the setup scripts -- `load_smtk.py`,
`modelbuilder_setup.py`, and, if applicable,
`docker_setup.py` -- each time you start modelbuilder.

The first step in creating a Wave Tank simulation is to
create a modelbuilder *project*, which essentially assigns
a directory for storing modelbuilder and OpenFOAM files.

All of the wave tank capabilities are accessed through the
modelbuilder "Operations" View.


1. If the "Operations" View is not visible, open the "View"
   menu and check the box next to the "Operations" item.

2. The "Operations" view displays a list sorted alphabetically.
   Find the entry "create_project.CreateProject", which is
   typically first in the list.

3. Double-click "create_project.CreateProject" to open an
   "Operation editor" at the bottom of the view.

4. In the editor, click the "Browse" button to navigate to
   an existing directory or (preferably) create a new
   directory for the project.

5. If you choose an existing directory for a new project,
   also check the box labeled "OK to overwrite
   existing directory?". Note that modelbuilder will erase
   all contents of the directory.

6. When you have entered the project directory, click the
   "Apply" button. In response, modelbuilder will create
   an "attribute resource" for specifying the simulation.

7. Open the "Attribute Editor" view and you should see 4
   top level tabs "Control", "blockMesh", "snappyHexMesh",
   and "interFoam".

8. To save the project, go back to the "Operations" view
   and find the smtk::project::Write operation. You will
   probably need to uncheck the "Limit by selection" box
   to see the list.

9. Double click the "smtk::project::Write" operation to
   open the "Operation editor". Select your project from the
   "Project" dropdown and click the "Apply" button.
