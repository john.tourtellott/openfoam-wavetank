Getting Started with OpenFOAM Wave Tank
========================================

This plugin works in conjunction with CMB modelbuilder to generate
case directories for OpenFOAM analysis of wave tank simulations.

For the initial development, a number of conditions are imposed on
end users, most notably:

* Access to the OpenFOAM software from modelbuilder must be via
  the Docker container software.
* The contents of this project are designed to work with the standard
  modelbuilder release packages. This will require more manual steps
  than with typical modelbuilder applications.


1\. Installing Prerequisites
=============================

1.1 Install Docker Engine
--------------------------

Make sure that Docker Engine is installed on your machine. You can
use either [Docker Desktop](https://docs.docker.com/desktop/) or
[Docker Engine](https://docs.docker.com/engine/install/binaries/).
(Note that Docker Desktop is a licensed application that, in some
cases, requires a paid subscription.)


1.2 Install openfoam-docker script
------------------------------------

OpenFOAM provides a shell script for pulling and running Docker images,
as well as mounting directories. Follow the instructions at
https://develop.openfoam.com/Development/openfoam/-/wikis/precompiled/docker
to download the `openfoam-docker` script and place it somewhere convenient
in your system's path.

* Be sure to `chmod +x openfoam-docker` after downloading.
* To pull the 2106 version of OpenFOAM, run `openfoam-docker 2106`.


1.3 Download modelbuilder package
-----------------------------------

The current modelbuilder release package (22.04) does not work with latest
Wave Tank software. For now, users should download a more recent version from
Kitware's nightly CI builds. The root folder for this is
https://data.kitware.com/#collection/58fa68228d777f16d01e03e5/folder/5980ed698d777f16d01ea0e8 .
Go to the folder with the most recent date and look for the package for your OS.
The choices are:

* `modelbuilder-22.04.-Linux-x86_64.tar.gz` for linux machines
* `modelbuilder-22.04.-OSX10.14-x86_64.dmg` for macOS/Intel machines
* `modelbuilder-22.04.-OSX11.0-arm64.dmg` for macOS/ARM64 machines
* `modelbuilder-22.04.-Windows-AMD64.zip` for Windows machines

In some cases, the nightly packages fail to build or upload. So if the package you
need is missing, try the previous date(s).

For linux and Windows machines, unpack the file in a convenient location.
The modelbuilder executable is in the `bin` directory. For macOS machines,
open the `.dmg` file and drag the modelbuilder application to a convenient
location in your file system.


1.4 Clone the OpenFOAM Wave Tank repository
---------------------------------------------

The source files for carrying out wave tank workflows are in
https://gitlab.kitware.com/cmb/plugins/openfoam-wavetank. Clone that
repository to a convenient place on your system.


2\. Starting modelbuilder
==========================

Each time you start modelbuilder, you must perform these manual steps to load the
wave tank operations.

2.1 Run load_smtk_op.py using "Export Simulation..." menu
----------------------------------------------------------

This script loads smtk python modules. (smtk is the `Simulation Modeling ToolKit`,
the main library in CMB.) These steps must be carried out *every time*
modelbuilder is run:

1. In the "File" menu, select the "Export Simulation..." item, which opens a
   file dialog.

2. In the file dialog, navigate to and select the `scripts/load_smtk_op.py` file
   in the wavetank repository.

3. The system will display another dialog with options to "Apply" or "Cancel".
   Click the "Apply" button.


2.2 Run modelbuilder_setup.py using Python Shell
-------------------------------------------------

This script registers a "wavetank" project type and associated operations. These steps
must be carried out *every time* modelbuilder is run:

1. In the "View" menu, find the "Python Shell" item and make sure its checkbox
   is checked. This opens and initializes a dock widget with a python prompt.

2. At the bottom of the "Python Shell" view, click the "Run Script" button
   and navigate to the `scripts/modelbuilder_setup.py` file in the wavetank
   repository.


2.3 Run docker_setup.py using Python Shell
-------------------------------------------

If you are using a NATIVE OpenFOAM INSTALL on your machine, then SKIP this step.
But if you are using `openfoam-docker` as described above, then also run the
`scripts/openfoam_setup.py` script in the Python Shell. This step must be
carried out *every time* modelbuilder is run.
