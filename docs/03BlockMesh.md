Setting up and running blockMesh
=================================

1\. Setup blockMesh
-------------------

1.1 In the "Attribute Editor", go to the "blockMesh" tab.
    You can edit the "Geometry" values, either in the numeric
    fields or by using the box widget in the 3-D render view.
    (The default values will work ok.)

1.2 Below the "Geometry" item is a "Mesh Size" item
    with different options for setting the mesh size. Todo
    document the options. You can edit that or use
    the default.


2\. Run blockMesh
-----------------

2.1 Go to the "Operations" view and find the "block_mesh.BlockMesh"
    item. Double-click the item to open the "Operation editor" at
    the bottom of the View.

2.2 Set the "Project" field to your project and click the "Apply" button.
    With the default settings, the operation should only take a few
    seconds.


3\. View the Mesh
-----------------

For now, you have to load the resulting mesh manually.

3.1 Enable ParaView toolbars, pipeline inspector, and other features
    by finding the toolbar icon that looks like the ParaView three-bar
    icon colored gray and click it. After clicking, the icon will
    change to the familiar red-green-blue colors, and many toolbars etc.
    are displayed.

3.2 Use the "File" => "Open" menu and navigate into your project directory,
    and select the file `<project-name>/foam/interFoam/backgroundMesh.foam`.
