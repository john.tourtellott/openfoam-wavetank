Writing and Reading Projects
=============================

0\. Selecting and Closing a Project
------------------------------------

Modelbuilder manages a collection of SMTK "resources". This particular
application uses SMTK *project* resources and SMTK *attribute* resources.
In order to close a project (remove it from memory), you must first select
it and then use the "File" => "Close Resource" menu item (Control W).

With the current implementation, the action of selecting a project is not
obvious.

The easiest way to select a project is to display the ParaView "Pipeline
Browser" view by clicking the ParaView-shaped icon in the toolbar. If there
is only one project loaded into memory, it will be the first item in the
"Pipeline Browser". So to close the project, click on that first item and
select the "File" => "Close Resource" menu item (or use Control W).


1\. Writing a Project
----------------------

After making changes in the "Attribute Editor", you should write your project to
the file system. The easiest way to do that is to select the project in the
"Pipeline Browser" and use the "File" => "Save Resource" menu item
(Control S).

Alternatively, you can also open the "Operations" view and use the
"smtk::project::Write" operation.


2\. Reading a Project
----------------------

Although you *can* read a project through the "Operations" view, we recommend
that you instead use the "File" => "Open" menu item, so that the project will be
included in the "File" => "Recent Files..." submenu.

2.1 In the "File" menu, select the "Open..." item to bring up a file dialog.

2.2 Navigate to your project directory.

2.3 Then navigate *into* your project directory and select the file
`<project-name>.project.smtk` and click the "OK" button.


3\. A Note About Multiple Projects
-----------------------------------

WE ADVISE AGAINST LOADING MORE THAN ONE MODELBUILDER PROJECT AT A TIME.
The modelbuilder software has not been tested with multiple projects; further,
it would be easy to get confused about which resource goes with which project.
So we recommend that you close your current project before creating or loading
a different project.
